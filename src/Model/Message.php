<?php

namespace Caltha\FreshmailIntegratorClient\Model;

abstract class Message implements MessageInterface
{
    private $source_id = '';
    private $from_email;
    private $from_name;
    private $reply_to_email;
    private $reply_to_name;
    private $email;
    private $subject;
    private $html;
    private $text;
    private $site_url;
    private $context = '';
    private $attachments = [];

    /**
     * @return string
     */
    public function getSourceId(): string
    {
        return $this->source_id;
    }

    /**
     * @param string $source_id
     */
    public function setSourceId(string $source_id): void
    {
        $this->source_id = $source_id;
    }

    /**
     * @return string
     */
    public function getFromEmail(): string
    {
        return $this->from_email;
    }

    /**
     * @param string $from_email
     */
    public function setFromEmail(string $from_email): void
    {
        $this->from_email = $from_email;
    }

    /**
     * @return string
     */
    public function getFromName(): string
    {
        return $this->from_name;
    }

    /**
     * @param string $from_name
     */
    public function setFromName(string $from_name): void
    {
        $this->from_name = $from_name;
    }

    /**
     * @return string
     */
    public function getReplyToEmail(): string
    {
        return (string) $this->reply_to_email;
    }

    /**
     * @param string $reply_to_email
     */
    public function setReplyToEmail(string $reply_to_email): void
    {
        $this->reply_to_email = $reply_to_email;
    }

    /**
     * @return string
     */
    public function getReplyToName(): string
    {
        return (string) $this->reply_to_name;
    }

    /**
     * @param string $reply_to_name
     */
    public function setReplyToName(string $reply_to_name): void
    {
        $this->reply_to_name = $reply_to_name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }

    /**
     * @param string $html
     */
    public function setHtml(string $html): void
    {
        $this->html = $html;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->site_url;
    }

    /**
     * @param string $site_url
     */
    public function setSiteUrl(string $site_url): void
    {
        $this->site_url = $site_url;
    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        return $this->context;
    }

    /**
     * @param string $context
     */
    public function setContext(string $context): void
    {
        $this->context = $context;
    }

    /**
     * @return array
     */
    public function getAttachments(): array {
        return $this->attachments;
    }

    /**
     * @param string $name File name with extension
     * @param string $content Content in Base64 format
     */
    public function addAttachment(string $name, string $content): void {
        $this->attachments[] = [
            'name' => $name,
            'content' => $content,
        ];
    }

    /**
     * @throws Exception
     */
    public function validate()
    {
        if(empty($this->getText()) && empty($this->getHtml())) {
            throw new \Exception('There is no message body content. At least one of variables text or html is necessary.');
        }
    }

}
