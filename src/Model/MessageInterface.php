<?php

namespace Caltha\FreshmailIntegratorClient\Model;

interface MessageInterface
{
    /**
     * Prepare Array with arguments send to new-message endpoint
     * @return array
     */
    public function prepareArray(): array;

    /**
     * Prepare JSON with arguments send to new-message endpoint
     * @return string
     */
    public function prepareJson(): string;

}
