<?php

namespace Caltha\FreshmailIntegratorClient\Model;

class MessageMailing extends Message
{

    /**
     * @return array
     */
    public function prepareArray(): array {
        $this->validate();
        return [
            "source_id" => $this->getSourceId(),
            "from_email" => $this->getFromEmail(),
            "from_name" => $this->getFromName(),
            "reply_to_email" => $this->getReplyToEmail(),
            "reply_to_name" => $this->getReplyToName(),
            "email" => $this->getEmail(),
            "subject" => $this->getSubject(),
            "html" => $this->getHtml(),
            "text" => $this->getText(),
            "attachments" => [],
            "site_url" => $this->getSiteUrl(),
            "context" => $this->getContext() ?: "CiviMail",
        ];
    }

    /**
     * @return string
     */
    public function prepareJson(): string {
        return json_encode($this->prepareArray());
    }

}