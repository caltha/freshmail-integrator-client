<?php

namespace Caltha\FreshmailIntegratorClient\Model;

use Exception;

class MessageSingle extends Message
{

    /**
     * @return array
     * @throws Exception
     */
    public function prepareArray(): array {
        $this->validate();
        return [
            "source_id" => $this->getSourceId() ?: sprintf('single.%s.%s', time(), bin2hex(random_bytes(3))),
            "from_email" => $this->getFromEmail(),
            "from_name" => $this->getFromName(),
            "reply_to_email" => $this->getReplyToEmail(),
            "reply_to_name" => $this->getReplyToName(),
            "email" => $this->getEmail(),
            "subject" => $this->getSubject(),
            "html" => $this->getHtml(),
            "text" => $this->getText(),
            "attachments" => $this->getAttachments(),
            "site_url" => $this->getSiteUrl(),
            "context" => $this->getContext() ?: "SingleEmail",
        ];
    }

    /**
     * @return string
     * @throws Exception
     */
    public function prepareJson(): string {
        return json_encode($this->prepareArray());
    }

}