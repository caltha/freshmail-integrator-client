<?php


namespace Caltha\FreshmailIntegratorClient\Service;

use Caltha\FreshmailIntegratorClient\Service\Configuration;
use Caltha\FreshmailIntegratorClient\Service\HttpClient\HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class ClientService extends HttpClient
{

    public function __construct(Configuration $configuration)
    {
        parent::__construct($configuration);
    }

    /**
     * @param string $json
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function createWithInstances(string $json): ResponseInterface {
        return $this->client->post('new-client', [
            RequestOptions::BODY => $json,
            RequestOptions::HEADERS => ['Content-Type' => 'application/json'],
            RequestOptions::HTTP_ERRORS => $this->configuration->getHttpErrors(),
        ]);
    }

}
