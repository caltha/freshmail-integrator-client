<?php

namespace Caltha\FreshmailIntegratorClient\Service;

class Configuration
{

    private $baseUrl = '';

    private $environment = '';

    private $secretKey = '';

    private $passphrase = '';

    private $privateKeyFile = '';

    private $publicKeyFile = '';

    private $algorithm = 'aes-128-ctr';

    private $httpErrors = true;

    /**
     * @return bool
     */
    public function getHttpErrors(): bool
    {
        return $this->httpErrors;
    }

    /**
     * Set value for option RequestOptions::HTTP_ERRORS
     * @param bool $httpErrors
     */
    public function setHttpErrors(bool $httpErrors): void
    {
        $this->httpErrors = $httpErrors;
    }

    /**
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param string $algorithm
     */
    public function setAlgorithm(string $algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @return string
     */
    public function getPassphrase(): string
    {
        return $this->passphrase;
    }

    /**
     * @param string $passphrase
     */
    public function setPassphrase(string $passphrase): void
    {
        $this->passphrase = $passphrase;
    }

    /**
     * @return string
     */
    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    /**
     * @param string $secretKey
     */
    public function setSecretKey(string $secretKey): void
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @return string
     */
    public function getPrivateKeyFile(): string
    {
        return $this->privateKeyFile;
    }

    /**
     * @param string $privateKeyFile
     */
    public function setPrivateKeyFile(string $privateKeyFile): void
    {
        $this->privateKeyFile = $privateKeyFile;
    }

    /**
     * @return string
     */
    public function getPublicKeyFile(): string
    {
        return $this->publicKeyFile;
    }

    /**
     * @param string $publicKeyFile
     */
    public function setPublicKeyFile(string $publicKeyFile): void
    {
        $this->publicKeyFile = $publicKeyFile;
    }

    public function __construct(string $environment)
    {
        $this->setEnvironment($environment);
    }

    /**
    * @return string
    */
    public function getEnvironment(): string
    {
        return $this->environment;
    }

    /**
    * @param string $environment
    */
    private function setEnvironment(string $environment): void
    {
        $this->environment = $environment;
    }

    /**
    * @return string
    */
    public function getBaseUrl(): string
    {
        if ($this->getEnvironment() == Environment::STAGING) {
          return 'https://fmi-staging.ce.civicrm.pl/';
        }
        if (!empty($this->baseUrl)) {
          return trim( $this->baseUrl, '/' ) . '/';
        }
        return 'https://fmi-production.ce.civicrm.pl/';
    }

  /**
   * @param string $baseUrl
   */
  public function setBaseUrl(string $baseUrl): void
  {
    $this->baseUrl = $baseUrl;
  }

}
