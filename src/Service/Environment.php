<?php

namespace Caltha\FreshmailIntegratorClient\Service;

final class Environment
{
    public const STAGING = 'staging';
    public const PRODUCTION = 'production';
}
