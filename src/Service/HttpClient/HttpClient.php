<?php
declare(strict_types=1);

namespace Caltha\FreshmailIntegratorClient\Service\HttpClient;

use GuzzleHttp\Client;
use Caltha\FreshmailIntegratorClient\Service\Configuration;

abstract class HttpClient {

    protected $configuration;
    protected $client;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
        $this->client = new Client([
            'base_uri' => $configuration->getBaseUrl(),
        ]);
    }

}
