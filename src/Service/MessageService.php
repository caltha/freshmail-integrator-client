<?php


namespace Caltha\FreshmailIntegratorClient\Service;

use Caltha\FreshmailIntegratorClient\Model\MessageInterface;
use Caltha\FreshmailIntegratorClient\Service\Configuration;
use Caltha\FreshmailIntegratorClient\Service\HttpClient\HttpClient;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class MessageService extends HttpClient
{

    public function __construct(Configuration $configuration)
    {
        parent::__construct($configuration);
    }

    /**
     * @param MessageInterface $message
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function send(MessageInterface $message): ResponseInterface {
        return $this->client->post('send-message', [
            RequestOptions::BODY => $message->prepareJson(),
            RequestOptions::HEADERS => ['Content-Type' => 'application/json'],
            RequestOptions::HTTP_ERRORS => $this->configuration->getHttpErrors(),
        ]);
    }

    /**
     * @param MessageInterface $message
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function sendEncrypted(MessageInterface $message): ResponseInterface {
        $cipher = $this->configuration->getAlgorithm();
        $ivLength = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivLength);
        $ciphertext = openssl_encrypt($message->prepareJson(), $cipher, $this->configuration->getSecretKey(), $options = 0, $iv);
        $publicKey = openssl_get_publickey(file_get_contents($this->configuration->getPublicKeyFile()));
        openssl_public_encrypt(
            $this->configuration->getSecretKey(),
            $encryptedSecretKey,
            $publicKey,
            OPENSSL_PKCS1_OAEP_PADDING
        );
        $encryptedData = sprintf('%s|||%s|||%s', $ciphertext, base64_encode($iv), base64_encode($encryptedSecretKey));
        return $this->client->post('send-message-encrypted', [
            RequestOptions::BODY => $encryptedData,
            RequestOptions::HEADERS => ['Content-Type' => 'text/plain'],
            RequestOptions::HTTP_ERRORS => $this->configuration->getHttpErrors(),
        ]);
    }

}
